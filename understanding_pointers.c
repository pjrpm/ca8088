/* Getting to know pointers
 * Author: Jose Pacheco
 * Adapted from: 
 */
#include <stdio.h>
int main(){
  int n;
  int* p; //using here an alternative notation

  n= 166; //asign a value to n
  p = &n; //Value of variable p is assigned the address of var n

  // Let's print those vars.
  printf("Value of var n is:%d\n",n); // Value of var n is:166
  printf("Value of var n is:%d\n",*p); // Value of var n is:166
  // Let's print what we were pointing at
  printf("n is located at %x.\n", &n); // n is located at bfbac2a8. 
  printf("n is located at %x.\n", p);  // n is located at bfbac2a8.
  // p variable has its own memory address, let's check it out
  printf("p is located at %x.\n",&p); // p is located at bff1ff88

return 0;
}

