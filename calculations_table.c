/* Program adapted from ABC book
 * Calculations on a table
 * By: Jose Pacheco
 * Nov 28, 2018
 */

#include <stdio.h>
int main(){
 float radius, diameter,circumference,area, pi;
 pi= 3.14159265f;

 printf("Input table diameter: ");
 scanf("%f", &diameter);
 radius = diameter / 2.0f;
 circumference = 2.0f*pi*radius;
 area = pi*radius*radius;
 printf("The circumference is %.2f\n", circumference);
 printf("\nThe area is: %.2f\n", area);
 
 return 0;
}
