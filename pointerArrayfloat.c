/* Pointer to a float array
 * Idea: Pointer arithmetic
 * Author: Jose Pacheco
 * Nov. 6 2018
 */
#include <stdio.h>
int main(){
float share[]={21.4, 33.0, 5.1,8.8};
float *p1, *p2, result;
p1 = p2 = share;
printf("pointer address: p1 %x, and p2 %x\n",&p1,&p2);
printf("pointer values: p1 %.2f, and p2 %.2f\n",*p1,*p2);
// operations
p1++;
p1++;
result = *p2 - *p1;
printf("pointer values: p1 %.2f, and p2 %.2f\n",*p1,*p2);
printf("p2- p1= %.2f\n",result);

}
