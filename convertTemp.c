/* Convierte temperatura
   solicita al usuario un valor de temperatura en F y los convierte a grados C
   Utiliza el concepto de definiciones propias de constantes
 */

#include <stdio.h>
#define FREEZING_PT 32.0f
#define SCALE_FACTOR (5.0f / 9.0f)

int main(){
  float fahrenheit, celsius;
  printf("Ingrese la temperatura en Fahrenheit: ");
  scanf("%f", &fahrenheit);
  celsius = (fahrenheit - FREEZING_PT)*SCALE_FACTOR;
  printf("Celsius equivalent: %0.2f\n", celsius);
  return 0;
  }
