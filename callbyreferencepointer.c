/*
 * Passing a pointer
 * Simulates call by referece
 * Adapted from: Johnsonbaugh & Kalin, 3rd ed.
 * By: Jose Pacheco
 * Nov, 2018
 */
#include <stdio.h>
#include <stdlib.h>

int main() {
    int num1 = 25;
    int num2,num3;
    int *p1 = &num1;
    printf("Value of var num1 = %d.\n",num1);
    int addition(int *q); //ask for a pointer
    num2 = addition(p1);  //passing a pointer

    // printing for learning
    printf("Value of var num1 = %d.\n",num1);
    printf("Value of var num1 via pointer %d.\n",*p1);
    printf("Value of var num1 address = %p.\n",&num1);

    // printing results
    printf("Value of num1, before adding %d.\n",*p1);
    printf("Value of num1, __after__ adding %d.\n",*p1);
    printf("Value of num2: %d\n",num2);
    num3 = addition(p1);
    printf("Value of num1, __after__ adding %d.\n",*p1);
    return 0;
}

/* function addition: expects the address of an integer (not an int*)
 * returns: an int
 */
int addition(int *q) {
  return ++*q; // it could've also be written as: ++*p
}


