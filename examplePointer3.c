/* 
Programa para probar el uso de punteros
Fecha: 03 de mayo de 2018
 
*/
#include <stdio.h>

int main(){
char c='O';
char d='H';
char *p1, *p2, *temp;
p1 = &c;
p2 = &d;
temp = p1;
p1 = p2;
p2 = temp;
printf("%c%c\n",*p1,*p2);
return 0;
}
