/*
Using pointers as arguments.
Call by reference or value tests
Author: Jose Pacheco
Date: may 20, 2018
rev: Nov 10, 2018
Note: This program has an academic purpose and not necessarily represents
      the best algorithm or programming strategies or methodologies.
*/
#include <stdio.h>

void two_operations(int x,int y, int* oper1, int* oper2) {
  printf("------------------------------------\n");
  printf("Calling two_operation function\n");
  printf("Value of oper2=%d\n",*oper2);
  printf("Address of oper2=%x\n",&oper2);
  printf("Value of oper1=%d\n",*oper1);
  printf("Address of oper2=%x\n",&oper1);
  printf("------------------------------------\n");
  *oper1 = x + y;
  *oper2 = x - y;
  printf("Value of oper2=%d\n",*oper2);
  printf("Address of oper2=%x\n",&oper2);
  printf("Value of oper1=%d\n",*oper1);
  printf("Address of oper2=%x\n",&oper1);
  printf("-----End of two_operations function ----\n");
 }

int main() {
  int a = 7;
  int b = 4;
  int addition, subtraction;
  printf("value of a=%d\n Value of b=%d\n",a,b);
  printf("address of a=%x\n address of b=%x\n",&a,&b);
  printf("address of addition=%x\n",&addition);
  printf("address of subtraction=%x\n",&subtraction);
  two_operations(a,b,&addition, &subtraction);
  printf("address of subtraction=%x\n",&subtraction);
  printf("value of subtraction=%d\n", subtraction);
  printf("Results: ---\n");
  printf("Addition(%d,%d)=%d --- Subtraction(%d,%d)=%d\n",a,b,addition,a,b,subtraction);
  return 0;
}
